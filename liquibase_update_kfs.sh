#!/bin/bash
#Update Database with Liquibase Changesets if they exist
export LIQUIBASE_BIN="`pwd`"
LIQUIBASE_CHANGELOG_DIR="ua-liquibase/kfs-core/src/main/resources/edu/arizona/kfs/db/changelog"
LIQUIBASE_TAG=ua-db-$(date +"%Y-%m-%d")

cd $LIQUIBASE_CHANGELOG_DIR

LIQUIBASE_STATUS=$("$LIQUIBASE_BIN/liquibase_kfs.sh" --changeLogFile=db.changelog-master.xml status)
 if [[ $LIQUIBASE_STATUS =~ "change sets have not been applied" ]]; then
  "$LIQUIBASE_BIN/liquibase_kfs.sh" --changeLogFile=db.changelog-master.xml update
  "$LIQUIBASE_BIN/liquibase_kfs.sh" tag $LIQUIBASE_TAG
 fi
