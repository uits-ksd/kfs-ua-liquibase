#!/bin/bash
# liquibase_kfs.sh: main liquibase script
# Calls liquibase with correct database parameters
# for this environment, path to ojdbc6.jar file.

LIQUIBASE_HOME="$LIQUIBASE_BIN/lib"
KFS_CONFIG_FILE="$LIQUIBASE_BIN/config.properties"

# username, password and url are assumed to be passed in from the environment
exec /usr/bin/java -jar "$LIQUIBASE_HOME/liquibase-3.3.5.jar" \
--url="$LIQUIBASE_DB_URL" \
--username=$LIQUIBASE_DB_USERNAME \
--password=$LIQUIBASE_DB_PASSWORD \
--classpath="$LIQUIBASE_HOME/ojdbc6.jar" \
--driver=oracle.jdbc.driver.OracleDriver \
--logLevel=info \
$@
