#!/bin/bash
rm -rf ua-liquibase
mkdir ua-liquibase
cd ua-liquibase
git init && \
    git config core.sparseCheckout true && \
    git remote add -f kfs https://github.com/ua-eas/kfs.git && \
    echo "kfs-core/src/main/resources/edu/arizona/kfs/db/changelog" > .git/info/sparse-checkout && \
    git checkout ua-development
cd ../
/bin/bash ./liquibase_update_kfs.sh
